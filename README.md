## Verdaccio-Auth

This is a Verdaccio Auth Plugin configured to authenticate users against the TeamStack API

## Installation

    npm install verdaccio-auth-stack

## Config

Add this to your `config.yaml`

    auth:
        auth-stack:
            url:
                auth: Authentication Endpoint
            group:
                enable: Boolean (Enable to check group level)
                level: Minimum Access Level Required
                name: Group name to return in callback (Required)

## Dependencies

1. [superagent](https://visionmedia.github.io/superagent)
