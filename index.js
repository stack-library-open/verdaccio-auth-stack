const request = require("superagent");

// Config
let CONFIG;

/**
 * Authenticate User
 */
async function authenticate(account, password, cb) {
    try {
        // Check credentials
        let res = await request
            .post(CONFIG.url.auth)
            .send({
                id: account,
                password: password
            });

        // Check access level
        if (Boolean(CONFIG.group.enable)) {
            // Fetch Account
            let account = res.body;
            // Check if empty or access level is lower
            if (!account || account.group < Number(CONFIG.group.level)) {
                throw new Error("Authentication failed");
            }
        }

        // All good
        cb(null, [
            CONFIG.group.name
        ]);
    } catch (err) {
        console.log(err);
        cb(null, false);
    }
}

/**
 * Add User
 */
async function adduser(user, password, callback) {
    return callback(null, true);
};

/**
 * Export
 */
module.exports = function(config, stuff) {
    // Save config
    CONFIG = config;
    // Return functions
    return {
        authenticate: authenticate,
        adduser: adduser
    };
};
