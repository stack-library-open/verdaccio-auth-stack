#!/bin/bash

NPM_AUTH_TOKEN=${1}

echo "registry=https://registry.npmjs.org/" > ~/.npmrc
echo "strict-ssl=true" >> ~/.npmrc
echo "//registry.npmjs.org/:_authToken=${NPM_AUTH_TOKEN}" >> ~/.npmrc
